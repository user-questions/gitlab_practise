package GitLab_Practise;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class demoTest {

	public WebDriver driver;
	 String username="Admin";
	 String password="admin123";
	 String website="https://opensource-demo.orangehrmlive.com/index.php/auth/login";
	static String parentWindow;
	
	@BeforeClass
	public void chromeInitialization() {
	 //System.setProperty("webdriver.chrome.driver","exe/chromedriver.exe");
	 
	 ChromeOptions ChromeOptions = new ChromeOptions();
     ChromeOptions.addArguments("--headless", "window-size=1024,768", "--no-sandbox");
     driver = new ChromeDriver(ChromeOptions);
	}
	@Test(priority=2)
	public void openWebsite()
	{
	  driver.get(website);
	  parentWindow=driver.getWindowHandle();
	}
	@Test(priority=3)
	public void maximizeWindow()
	{
	  driver.manage().window().maximize();
	}
	
	@Test(priority=4)
	public void login()
	{
		driver.switchTo().window(parentWindow);
		driver.findElement(By.id("txtUsername")).sendKeys(username);
		driver.findElement(By.id("txtPassword")).sendKeys(password);
		driver.findElement(By.id("btnLogin")).click();
		
	}
	@AfterClass
	public void quitBrowser()
	{
		driver.quit();
	}
}
