package GitLab_Practise.GitLab_Practise;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class demoPurpose {
	public WebDriver driver;
	 String username="Admin";
	 String password="admin123";
	 String website="https://opensource-demo.orangehrmlive.com/index.php/auth/login";
	static String parentWindow;
	public void chromeInitialization() {
	 System.setProperty("webdriver.chrome.driver","C:\\Selenium\\chromedriver.exe");
	 driver = new ChromeDriver();
	}
	

	public void openWebsite()
	{
	  driver.get(website);
	  parentWindow=driver.getWindowHandle();
	}
	

	public void login()
	{
	  driver.manage().window().maximize();
	 driver.findElement(By.xpath("//*[@id='txtUsername']")).sendKeys(username);
	driver.findElement(By.id("txtPassword")).sendKeys(password);
		driver.findElement(By.id("btnLogin")).click();
	}
	
	
	public void validateDashboard()
	{
		String dashboard="Welcome";
		String val=driver.findElement(By.xpath("//*[contains(.='welcome')]")).getText();
		Assert.assertEquals(dashboard, val);
		
	}
	

	public void quitBrowser()
	{
		driver.quit();
	}
}
